import tensorflow as tf

"""Data Pre-occessing functions"""


# Redundant :(
def get_weight_var(layer_name):
    # Retrieve an existing tf.variable named 'kernel' in the scope with the given name
    with tf.variable_scope(layer_name, reuse=True):
        variable = tf.get_variable('kernel')

    return variable


def create_datasets(data):
    x = data.images
    y = data.labels

    dx = tf.data.Dataset.from_tensor_slices(x)
    dy = tf.data.Dataset.from_tensor_slices(y)

    return tf.data.Dataset.zip((dx, dy)).shuffle(500).repeat().batch(15)