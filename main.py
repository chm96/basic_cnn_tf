import tensorflow as tf
import aux_fn as af
import models as m
from tensorflow.examples.tutorials.mnist import input_data

"""Data pre-pre-proccesing - Hand draw number """
data = input_data.read_data_sets('data/MNIST/', one_hot=True)

train_dataset = af.create_datasets(data.train)
test_dataset = af.create_datasets(data.test)


"""Iterators"""
# define iterator
iterator = tf.data.Iterator.from_structure(train_dataset.output_types, train_dataset.output_shapes)
next_element = iterator.get_next()

# op to point to datasets whilst running model
training_init_op = iterator.make_initializer(train_dataset)
validation_init_op = iterator.make_initializer(test_dataset)

"""Train Model"""
logits = m.basic_cnn_model(next_element[0])

"""Teach the model"""
# find index of highest ( most likely ) output
label_pred_cls = tf.argmax(logits, dimension=1)

"""batch size twice due to img flipping in model"""
labels = next_element[1]
labels = tf.concat([labels, labels], 0)

# evaluating precision of output
cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=logits)

loss = tf.reduce_mean(cross_entropy)
tf.summary.histogram('loss', loss)
global_step = tf.contrib.framework.get_or_create_global_step()

# optimiser
optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(loss, global_step=global_step)

correct_prediction = tf.equal(label_pred_cls, tf.argmax(labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
tf.summary.histogram('accuracy', accuracy)

""" Rum Program """
iters = 600
config = tf.ConfigProto()
hooks = [tf.train.StopAtStepHook(last_step=20300)]
merge = tf.summary.merge_all()
with tf.train.MonitoredTrainingSession(checkpoint_dir='./Checkpoints/', hooks=hooks, config=config,
                                       save_checkpoint_secs=500) as mon_sess:

    # add a writer for tensorboard
    train_writer = tf.summary.FileWriter('./logs/1/train ', mon_sess.graph)

    while not mon_sess.should_stop():

        mon_sess.run(training_init_op)
        for i in range(iters):

            summary, l, _, acc = mon_sess.run([merge, loss, optimizer, accuracy])
            train_writer.add_summary(summary, i)
            if i % 50 == 0:
                msg = "T - Epoch: {}, Acc: {:.2f}%"
                print(msg.format(i + 1, acc*100))

        # Validation / Testing run
        valid_iters = 100
        mon_sess.run(validation_init_op)
        avg_acc = 0
        for i in range(valid_iters):
            acc = mon_sess.run([accuracy])
            avg_acc += acc[0]

        msg = "V - Epoch: {}, Acc: {:.2f}%\n"
        print(msg.format(valid_iters, (avg_acc / valid_iters)*100))
        break
